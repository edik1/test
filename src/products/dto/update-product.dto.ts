export class updateProductDto{
    readonly title: string
    readonly price: number
    readonly description: string
}